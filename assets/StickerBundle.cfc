component output=false {

	public void function configure( bundle ) output=false {
        
        // baguettebox
        bundle.addAsset( id="js-baguettebox", url="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.min.js" );
        bundle.addAsset( id="css-baguettebox", url="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.css" );
		
	}
}