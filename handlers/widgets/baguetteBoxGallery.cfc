component {

        property name="presideObjectService" inject="presideObjectService";        

	private function index( event, rc, prc, args={} ) {

        args.galleryImages = presideObjectService.selectData(
                objectName   = "asset"
                , filter = { asset_folder = args.galleryFolder, is_trashed = 0 }
        );
        
        return renderView( view="widgets/baguetteBoxGallery/index", args=args ); 

	}
}
