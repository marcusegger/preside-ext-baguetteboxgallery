# Baguettebox JS Gallery Widget for Preside

## General
This extension provides you with a widget to place image galleries in your Preside pages (main_content and other richtext fields). This extension uses the awseome [Github project "baguetteBox.js"](https://github.com/feimosi/baguetteBox.js).

## Usage
After installing the extension and reloading your [Preside application](https://www.preside.org/) you have a new widget "baguettboxGallery" available through the widget-picker. Configure options are a Gallery-Title, an asset folder and your choice of dispalying the gallery title or not.