<cfscript>
	event.include( "css-baguettebox" ).include( "js-baguettebox");
</cfscript>

<cfoutput>
    <script>
        window.addEventListener('load', function() {
            baguetteBox.run('.baguetteBox_#args.galleryFolder#', {
                animation: 'fadeIn',
                noScrollbars: true,
                preload: 8,
                filter: /.*/i
            });
        });    
    </script>

    <div class="container px-0 text-center">
        <cfif args.showGalleryTitle eq 1>
            <h2>#args.galleryTitle#</h2>
        </cfif>
        <div class="baguetteBox_#args.galleryFolder# gallery">
            <cfloop query="args.galleryImages">
                <cfset currentGalleryImage = event.buildLink(assetId=id, derivative="galleryFull") />
                <a href="#currentGalleryImage#" data-caption="#description#" style="display:inline-block;margin-bottom:5px;">
                    #renderAsset(assetId=id, args={ derivative="galleryThumbnail" })#
                </a>
            </cfloop>
        </div>
    </div>

</cfoutput>